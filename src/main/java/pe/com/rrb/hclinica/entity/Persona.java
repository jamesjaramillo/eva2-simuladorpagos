package pe.com.rrb.hclinica.entity;
/*
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tmPersona")
@Data
@NoArgsConstructor
public class Persona {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long idPersona;
	
	@Column(name = "tipoDocumento", nullable = false)
	private String tipoDocumento;
	
	@Column(name = "nroDocumento", nullable = false)
	private String nroDocumento;
	
	@Column(name = "apPaterno", nullable = false)
	private String apPaterno;
	
	@Column(name = "apMaterno", nullable = false)
	private String apMaterno;
	
	@Column(name = "nombres", nullable = false)
	private String nombres;
	
	@Column(name = "fechaNacimiento", nullable = false)
	private Date fechaNacimiento;
	
	@Column(name = "tipoSexo", nullable = false)
	private String tipoSexo;
	
	@Column(name = "tipoEstadoCivil", nullable = false)
	private String tipoEstadoCivil;
	
	@Column(name = "alergia", nullable = false)
	private String alergia;
	
	@Column(name = "intervencionesQuirurgicas", nullable = false)
	private String intervencionesQuirurgicas;
	
	@Column(name = "vacunasCompletas", nullable = false)
	private int vacunasCompletas;
	
	@Column(name = "direccion", nullable = false)
	private String direccion;
	
	@Column(name = "telefono", nullable = false)
	private String telefono;
	
	@Column(name = "email", nullable = false)
	private String email;
	
	@Column(name = "ocupacion", nullable = false)
	private String ocupacion;
	
	@Column(name = "personaResponsable", nullable = false)
	private String personaResponsable;


}*/
