package pe.com.rrb.hclinica.dto;

import java.util.Date;

import lombok.Data;

@Data
public class PersonaDTO {

	private Long idPersona;
	private String tipoDocumento;	
	private String nroDocumento;	
	private String apPaterno;	
	private String apMaterno;	
	private String nombres;	
	private Date fechaNacimiento;	
	private String tipoSexo;	
	private String tipoEstadoCivil;	
	private String alergia;	
	private String intervencionesQuirurgicas;	
	private int vacunasCompletas;	
	private String direccion;		
	private String telefono;	
	private String email;	
	private String ocupacion;	
	private String personaResponsable;

}
