package pe.com.rrb.hclinica.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.test.util.DataUtil;

@RestController
@RequestMapping(path = "/COREMS3K/servicios/servicio")
public class PortalesController {
	private static int CONTADOR=0;
	
	@PostMapping(path = "/obtenerOperatividadKiosko", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerOperatividadKiosko(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /obtenerOperatividadKiosko");
		String tramaJson="{\"descripcion\":\"ok\",\"estado\":\"0\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/costoServicioPortales", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> costoServicioPortales(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /costoServicioPortales");
		String tramaJson="{\"descripcion\":\"ok\",\"fechaIngreso\":\"15-10-2018 09:35:02\",\"estado\":\"0\",\"codigoEntrada\":\"01\",\"monto\":\"0.0\",\"impuesto\":\"0.0\",\"tiempo\":\"68\",\"fechaSalida\":\"15-10-2018 10:43:00\",\"nroTicket\":\"001597\",\"montoTotal\":\"2.0\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		
		CONTADOR=0;
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/consultarEfectivoIngresado", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> consultarEfectivoIngresado(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /consultarEfectivoIngresado \t CONTADOR "+CONTADOR);
		String monto=DataUtil.getMonto().get(CONTADOR);
		String tramaJson="{\"estado\":\"3\",\"descripcion\":\"ok\",\"datosUsuario\":\"\",\"monto\":\""+monto+"\",\"estadoDispositivo\":\"\",\"mensajeImprimirDevolucion\":\"\",\"descripcionAux\":\"\",\"billetes\":\"\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		CONTADOR++;
		try {
			Thread.sleep(245);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/habilitarIngresoDeBilletes", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> habilitarIngresoDeBilletes(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /habilitarIngresoDeBilletes");
		String tramaJson="{\"estado\":\"0\",\"descripcion\":\"OK\",\"datosUsuario\":\"\",\"monto\":\"0.0\",\"estadoDispositivo\":\"\",\"mensajeImprimirDevolucion\":\"\",\"descripcionAux\":\"\",\"billetes\":[{\"denominacion\":100,\"cantidad\":0,\"estado\":0,\"descripcion\":\"PEN\",\"idNote\":\"\",\"cantidadPayout\":0,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"billetesCashBox\":\"\",\"billetes\":[]},{\"denominacion\":200,\"cantidad\":0,\"estado\":0,\"descripcion\":\"PEN\",\"idNote\":\"\",\"cantidadPayout\":0,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"billetesCashBox\":\"\",\"billetes\":[]},{\"denominacion\":50,\"cantidad\":0,\"estado\":0,\"descripcion\":\"PEN\",\"idNote\":\"\",\"cantidadPayout\":0,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"billetesCashBox\":\"\",\"billetes\":[]},{\"denominacion\":20,\"cantidad\":0,\"estado\":0,\"descripcion\":\"PEN\",\"idNote\":\"\",\"cantidadPayout\":0,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"billetesCashBox\":\"\",\"billetes\":[]},{\"denominacion\":10,\"cantidad\":1,\"estado\":0,\"descripcion\":\"PEN\",\"idNote\":\"\",\"cantidadPayout\":1,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"billetesCashBox\":\"\",\"billetes\":[]}]}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		Thread.sleep(5000);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	
	@PostMapping(path = "/invocarLuces", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> invocarLuces(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /invocarLuces");
		String tramaJson="{\"descripcion\":\"ok\",\"estado\":\"0\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/desactivarDispositivos", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> desactivarDispositivos(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /desactivarDispositivos");
		/*String tramaJson="{\"descripcion\":\"ok\",\"estado\":\"0\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);*/
		
		return ResponseEntity.status(HttpStatus.OK).body(1);
	}
	
	@PostMapping(path = "/cancelarUltimoComando", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> cancelarUltimoComando(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /cancelarUltimoComando");
		/*String tramaJson="{\"descripcion\":\"ok\",\"estado\":\"0\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);*/
		
		return ResponseEntity.status(HttpStatus.OK).body(0);
	}
	
	@PostMapping(path = "/procesarPago", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> procesarPago(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /procesarPago");
		String tramaJson="{\"estado\":\"0\",\"descripcion\":\"pago realizado con exito\",\"datosUsuario\":\"\",\"textoImprimir\":\"13\\u001CICPortalesLogo.png\\u001C13\\u001C13\\u001C22Los Portales\\u001C21             SEDE PRINCIPAL             \\u001C21     Av. de las AmÃ©ricas #57, BogotÃ¡     \\u001C23\\u001C22NOTA DE CREDITO \\u001C13\\u001C21TERM:   20170917       REF  : 002304\\u001C21FECHA:  26OCT2021      HORA  :  14:33\\u001C13\\u001C11________________________________________\\u001C13\\u001C23PRODUCTO        :   EFECTIVO\\u001C23CLIENTE         :   452741xx\\u001C11________________________________________\\u001C13\\u001C23COSTO SERVICIO  :     S/.    14.50\\u001C23MONTO INGRESADO :     S/.    16.00\\u001C23VUELTO          :     S/.     1.50\\u001C13\\u001C13\\u001C13\\u001CCP\\u001C\",\"vuelto\":\"0\",\"textoImprimirVuelto\":\"13\\u001CICPortalesLogo.png\\u001C13\\u001C13\\u001C22Los Portales\\u001C21             SEDE PRINCIPAL             \\u001C21     Av. de las AmÃ©ricas #57, BogotÃ¡     \\u001C23\\u001C22NOTA DE CREDITO \\u001C13\\u001C21TERM:   20170917       REF  : 002304\\u001C21FECHA:  26OCT2021      HORA  :  14:33\\u001C13\\u001C11________________________________________\\u001C13\\u001C23PRODUCTO        :   EFECTIVO\\u001C23CLIENTE         :   452741xx\\u001C11________________________________________\\u001C13\\u001C23COSTO SERVICIO  :     S/.    14.50\\u001C23MONTO INGRESADO :     S/.    16.00\\u001C23VUELTO          :     S/.     1.50\\u001C13\\u001C13\\u001C13\\u001CCP\\u001C\",\"monedas\":\"\",\"dispensaBilletes\":\"\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	
	@PostMapping(path = "/pagoPortales", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> pagoPortales(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /pagoPortales");
		String tramaJson="{\"descripcion\":\"ok\",\"estado\":\"0\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/entregarDinero", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> entregarDinero(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /entregarDinero");
		String tramaJson="{\"estado\":\"0\",\"descripcion\":\"dispenso ok\",\"datosUsuario\":\"\",\"textoImprimir\":\"13\\u001CICPortalesLogo.png\\u001C13\\u001C13\\u001C22Los Portales\\u001C21             SEDE PRINCIPAL             \\u001C21     Av. de las AmÃ©ricas #57, BogotÃ¡     \\u001C23\\u001C22NOTA DE CREDITO \\u001C13\\u001C21TERM:   20170917       REF  : 002304\\u001C21FECHA:  26OCT2021      HORA  :  14:35\\u001C13\\u001C11___________________________________________\\u001C13\\u001C23PRODUCTO        :   EFECTIVO\\u001C23NRO DE TICKET   :     001597\\u001C11___________________________________________\\u001C13\\u001C23MONTO INGRESADO    :    S/   16.00\\u001C23MONTO DEVUELTO     :    S/    0.00\\u001C23MONTO POR DEVOLVER :    S/    1.50\\u001C13\\u001C21POR FAVOR ACERCATE A UN ANFITRION PARA\\u001C21SOLUCIONAR EL INCONVENIENTE\\u001C13\\u001C13\\u001CCP\\u001C\",\"vuelto\":\"1,50\",\"textoImprimirVuelto\":\"13\\u001CICPortalesLogo.png\\u001C13\\u001C13\\u001C22Los Portales\\u001C21             SEDE PRINCIPAL             \\u001C21     Av. de las AmÃ©ricas #57, BogotÃ¡     \\u001C23\\u001C22NOTA DE CREDITO \\u001C13\\u001C21TERM:   20170917       REF  : 002304\\u001C21FECHA:  26OCT2021      HORA  :  14:35\\u001C13\\u001C11___________________________________________\\u001C13\\u001C23PRODUCTO        :   EFECTIVO\\u001C23NRO DE TICKET   :     001597\\u001C11___________________________________________\\u001C13\\u001C23MONTO INGRESADO    :    S/   16.00\\u001C23MONTO DEVUELTO     :    S/    0.00\\u001C23MONTO POR DEVOLVER :    S/    1.50\\u001C13\\u001C21POR FAVOR ACERCATE A UN ANFITRION PARA\\u001C21SOLUCIONAR EL INCONVENIENTE\\u001C13\\u001C13\\u001CCP\\u001C\",\"monedas\":\"\",\"dispensaBilletes\":\"\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	
	@PostMapping(path = "/voucherPortales", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> voucherPortales(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /voucherPortales");
		String tramaJson="{\"descripcion\":\"ok\",\"estado\":\"0\",\"print_data\":\"13\\u001CICPortalesLogo.png\\u001C13\\u001C22LOS PORTALES SA\\u001C21RUC: 20301837896\\u001C21Jr. Mariscal La Mar Nro. 991\\u001C21Magdalena del Mar - Lima\\u001C21Calle El Parque 150 - San Isidro\\u001C21 FACTURA DE VENTA ELECTRONICA\\u001C21005-00040002\\u001C21Fecha: 19/12/2018  Hora: 16:39\\u001C21Ticket: 01-001592\\u001C21H. Ing.: 19/12/2018 16:33:52\\u001C21H. Sal.: 19/12/2018 16:37:40\\u001C21Tarifa: PACIENTE\\u001C21Cajero: APS 3\\u001C21Doc. Identidad: RUC\\u001C21Numero: 10405179097\\u001C21HIPER S.A.\\u001C13\\u001C13\\u001CCP\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/realizarImpresion", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> realizarImpresion(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /realizarImpresion");
		String tramaJson="{\"estado\":\"1\",\"descripcion\":\"IMPRESIoN CORRECTA\",\"datosUsuario\":\"\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	
	@PostMapping(path = "/setearEstadoKiosko", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> setearEstadoKiosko(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /setearEstadoKiosko");
		String tramaJson="{\"descripcion\":\"Equipo en reposo \",\"estado\":\"0\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/generarTrace", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> generarTrace(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /generarTrace");
		String tramaJson="{\"estado\":\"0\",\"descripcion\":\"002305\",\"datosUsuario\":\"\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/validarDispositivos", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> validarDispositivos(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /validarDispositivos");
		String tramaJson="";
		List<HashMap<String, Object>> dataList=new ArrayList<HashMap<String,Object>>();
		if(input.get("accion").equals("sensor00")) {
			tramaJson="{\"estado\":\"1\",\"descripcion\":\"Cerrada\",\"datosUsuario\":\"\",\"dispositivo\":\"PUERTA\"}";
			JSONObject json=new JSONObject(tramaJson);
			HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
			dataList.add(data);
		}
		else {
			String tramaJson1="{\"estado\":\"0\",\"descripcion\":\"Activo\",\"datosUsuario\":\"\",\"dispositivo\":\"ACEPTADOR DE MONEDAS\"}";
			String tramaJson2="{\"estado\":\"0\",\"descripcion\":\"Activo\",\"datosUsuario\":\"\",\"dispositivo\":\"RECICLADOR DE BILLETES\"}";
			String tramaJson3="{\"estado\":\"0\",\"descripcion\":\"Activo\",\"datosUsuario\":\"\",\"dispositivo\":\"IMPRESORA\"}";
			
			JSONObject json=new JSONObject(tramaJson1);
			HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
			dataList.add(data);
			
			json=new JSONObject(tramaJson2);
			HashMap<String, Object> data2=(HashMap<String, Object>) jsonToMap(json);
			dataList.add(data2);
			
			json=new JSONObject(tramaJson3);
			HashMap<String, Object> data3=(HashMap<String, Object>) jsonToMap(json);
			dataList.add(data3);
		}
		
		
		
		return ResponseEntity.status(HttpStatus.OK).body(dataList);
	}
	
	@PostMapping(path = "/validarTicketAdmin", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> validarTicketAdmin(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /validarTicketAdmin");
		String tramaJson="{\"estado\":\"0\",\"encontrado\":false}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		List<HashMap<String, Object>> dataList=new ArrayList<HashMap<String,Object>>();
		dataList.add(data);
		
		return ResponseEntity.status(HttpStatus.OK).body(dataList);
	}
	
	
	@PostMapping(path = "/habilitarIngresoEfectivo", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> habilitarIngresoEfectivo(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /habilitarIngresoEfectivo \t CONTADOR "+CONTADOR);
		String tramaJson="{\"estado\":\"0\",\"descripcion\":\"Ok\",\"datosUsuario\":\"\",\"monto\":\"\",\"estadoDispositivo\":\"\",\"mensajeImprimirDevolucion\":\"\",\"descripcionAux\":\"15\",\"billetes\":\"\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) jsonToMap(json);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	
	
	
	
	
	
	
	
	public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
	    Map<String, Object> retMap = new HashMap<String, Object>();
	    
	    if(json != JSONObject.NULL) {
	        retMap = toMap(json);
	    }
	    return retMap;
	}

	public static Map<String, Object> toMap(JSONObject object) throws JSONException {
	    Map<String, Object> map = new HashMap<String, Object>();

	    Iterator<String> keysItr = object.keys();
	    while(keysItr.hasNext()) {
	        String key = keysItr.next();
	        Object value = object.get(key);
	        
	        if(value instanceof JSONArray) {
	            value = toList((JSONArray) value);
	        }
	        
	        else if(value instanceof JSONObject) {
	            value = toMap((JSONObject) value);
	        }
	        map.put(key, value);
	    }
	    return map;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
	    List<Object> list = new ArrayList<Object>();
	    for(int i = 0; i < array.length(); i++) {
	        Object value = array.get(i);
	        if(value instanceof JSONArray) {
	            value = toList((JSONArray) value);
	        }

	        else if(value instanceof JSONObject) {
	            value = toMap((JSONObject) value);
	        }
	        list.add(value);
	    }
	    return list;
	}
}
