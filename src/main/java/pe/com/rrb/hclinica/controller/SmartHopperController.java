package pe.com.rrb.hclinica.controller;

import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.test.util.UtilMap;

@RestController
@RequestMapping(path = "/API_SmartHopper/resources/ServicioSmartHopper")
public class SmartHopperController {

	@PostMapping(path = "/estadoDispositivo", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> aceptarBilletes(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO estadoDispositivo");
		String tramaJson = "{\"descripcion\":\"Dispositivo operativo\",\"estado\":\"0\"}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(500);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/agregarMonedas", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> agregarMonedas(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO agregarMonedas");
		String tramaJson = "{\"descripcion\":\"Denominacion actualizada correctamente\",\"estado\":\"0\"}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(500);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/vaciarHopper", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> vaciarHopper(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO agregarMonedas");
		String tramaJson = "{\"descripcion\":\"vaciado correctamente\",\"estado\":\"0\"}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(5000);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
}
