package pe.com.rrb.hclinica.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.test.util.UtilMap;

@RestController
@RequestMapping(path = "/API_Billetero/resources/ServicioBilletero")
public class BilleteroController {

	@PostMapping(path = "/aceptarBilletes", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> aceptarBilletes(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO aceptarBilletes");
		String tramaJson = "{\"billetes\":[{\"billetes\":[],\"billetesCashBox\":null,\"cantidad\":0,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"cantidadPayout\":0,\"denominacion\":100,\"descripcion\":\"PEN\",\"estado\":0,\"idNote\":null},{\"billetes\":[],\"billetesCashBox\":null,\"cantidad\":0,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"cantidadPayout\":0,\"denominacion\":200,\"descripcion\":\"PEN\",\"estado\":0,\"idNote\":null},{\"billetes\":[],\"billetesCashBox\":null,\"cantidad\":0,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"cantidadPayout\":0,\"denominacion\":50,\"descripcion\":\"PEN\",\"estado\":0,\"idNote\":null},{\"billetes\":[],\"billetesCashBox\":null,\"cantidad\":0,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"cantidadPayout\":0,\"denominacion\":20,\"descripcion\":\"PEN\",\"estado\":0,\"idNote\":null},{\"billetes\":[],\"billetesCashBox\":null,\"cantidad\":1,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"cantidadPayout\":1,\"denominacion\":10,\"descripcion\":\"PEN\",\"estado\":0,\"idNote\":null}],\"billetesCashBox\":null,\"cantidad\":0,\"cantidadCashbox\":0,\"cantidadDispensada\":0,\"cantidadNoDispensada\":0,\"cantidadPayout\":0,\"denominacion\":0,\"descripcion\":\"Ok\",\"estado\":0,\"idNote\":null}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(500);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/obtenerEstadoEquipo", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerEstadoEquipo(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO obtenerEstadoEquipo");
		String tramaJson = "{\"descripcion\":\"Ok\",\"estado\":\"0\"}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(500);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/cancelarUltimoComando", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> cancelarUltimoComando(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO cancelarUltimoComando");
		String tramaJson = "{\"descripcion\":\"Ok\",\"estado\":\"0\"}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(500);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/actualizarComponentes", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> actualizarComponentes(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO cancelarUltimoComando");
		String tramaJson = "{\"descripcion\":\"Ok\",\"estado\":\"0\"}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(500);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/vaciarPayout", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> vaciarPayout(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO vaciarPayout");
		String tramaJson = "{\"descripcion\":\"Dispositivo purgado correctamente\",\"estado\":\"0\"}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(500);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	
}
