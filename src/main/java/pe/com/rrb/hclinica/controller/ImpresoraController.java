package pe.com.rrb.hclinica.controller;

import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.test.util.UtilMap;

@RestController
@RequestMapping(path = "/API_Impresora/resources/ServicioImpresoraTermica/")
public class ImpresoraController {

	@PostMapping(path = "/testearImpresora", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> apagarLuces(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /testearImpresora");
		String tramaJson="{\"descripcion\":\"IMPRESORA OPERATIVA\",\"estado\":\"0\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) UtilMap.jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/imprimir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> imprimir(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /imprimir");
		String tramaJson="{\"descripcion\":\"IMPRESION OK\",\"estado\":\"1\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) UtilMap.jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
}
