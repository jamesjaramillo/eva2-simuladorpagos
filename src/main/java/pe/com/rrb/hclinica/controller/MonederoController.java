package pe.com.rrb.hclinica.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.test.util.DataUtil;
import pe.test.util.UtilMap;

@RestController
@RequestMapping(path = "/API_Aceptador/resources/ServicioAceptador")
public class MonederoController {

	private static int CONTADOR=0;
	
	@PostMapping(path = "/habilitarAceptador", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> habilitarAceptador(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /habilitarAceptador");
		String tramaJson="{\"descripcion\":\"ACEPTADOR DE MONEDAS HABILITADO\",\"estado\":\"1\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) UtilMap.jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/obtenerEstadoAceptador", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerEstadoAceptador(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /obtenerEstadoAceptador");
		String tramaJson="{\"descripcion\":\"ACEPTADOR DE MONEDAS OPERATIVO\",\"estado\":\"0\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) UtilMap.jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/obtenerMonedasIngresadas", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerMonedasIngresadas(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /obtenerMonedasIngresadas");
		String monto=DataUtil.getMonto().get(CONTADOR);
		System.out.println("monto -> "+monto+"\t CONTADOR "+CONTADOR);
		String tramaJson="{\"descripcion\":\""+monto+"\",\"estado\":\"1\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) UtilMap.jsonToMap(json);
		
		CONTADOR++;
		if(CONTADOR>=DataUtil.getMonto().size()) {
			CONTADOR=0;
		}
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	//{"descripcion":"ACEPTADOR DE MONEDAS CERRADO","estado":"1"}
	
	@PostMapping(path = "/bloquearAceptador", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> bloquearAceptador(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /bloquearAceptador");
		String tramaJson="{\"descripcion\":\"ACEPTADOR DE MONEDAS CERRADO\",\"estado\":\"1\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) UtilMap.jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	
	
	
	
	
	
	
}
