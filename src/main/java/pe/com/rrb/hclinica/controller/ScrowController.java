package pe.com.rrb.hclinica.controller;

import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.test.util.UtilMap;

@RestController
@RequestMapping(path = "/API_Scrow/resources/ServicioScrow")
public class ScrowController {
	
	@PostMapping(path = "/aceptar", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> aceptarBilletes(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO aceptar");
		String tramaJson = "{\"descripcion\":\"Aceptar OK\",\"estado\":\"1\"}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(500);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/devolver", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> devolver(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO devolver");
		String tramaJson = "{\"descripcion\":\"Devolver OK\",\"estado\":\"1\"}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(500);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/testear", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> testear(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO testear");
		String tramaJson = "{\"descripcion\":\"Testear OK\",\"estado\":\"1\"}";
		JSONObject json = new JSONObject(tramaJson);
		System.out.println("salio de la conversion");
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		Thread.sleep(500);
		System.out.println("data convertidad -> "+data);
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}
