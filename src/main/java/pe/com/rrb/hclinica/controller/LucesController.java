package pe.com.rrb.hclinica.controller;

import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.test.util.UtilMap;

@RestController
@RequestMapping(path = "/API_Luces/resources/ServicioLuces")
public class LucesController {
	
	@PostMapping(path = "/encenderLuces", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> encenderLuces(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /encenderLuces");
		String tramaJson="{\"descripcion\":\"LUCES(on) ACEPTADORMONEDAS OK\",\"estado\":\"1\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) UtilMap.jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/apagarLuces", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> apagarLuces(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /apagarLuces");
		String tramaJson="{\"descripcion\":\"LUCES(off) ACEPTADORMONEDAS OK\",\"estado\":\"1\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data=(HashMap<String, Object>) UtilMap.jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
}
