package pe.com.rrb.hclinica.controller;

import java.util.HashMap;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.test.util.UtilMap;

@RestController
@RequestMapping(path = "/API_Hopper/resources/ServicioHopper")
public class HopperController {
	
	@PostMapping(path = "/obtenerEstadoEquipo", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerEstadoEquipo(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /obtenerEstadoEquipo");
		String tramaJson="{\"descripcion\":\"ok\",\"estado\":\"0\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}
	
	@PostMapping(path = "/dispensar", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> dispensar(@RequestBody HashMap<String, Object> input) throws Exception {
		System.out.println("INGRESO AL SERVICIO /dispensar");
		String tramaJson="{\"descripcion\":\"0 MONEDAS NO DISPENSADAS\",\"estado\":\"DISPENSACION CORRECTA\"}";
		JSONObject json=new JSONObject(tramaJson);
		HashMap<String, Object> data = (HashMap<String, Object>) UtilMap.jsonToMap(json);
		
		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}
