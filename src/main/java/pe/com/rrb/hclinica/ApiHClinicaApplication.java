package pe.com.rrb.hclinica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiHClinicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiHClinicaApplication.class, args);
	}

}
