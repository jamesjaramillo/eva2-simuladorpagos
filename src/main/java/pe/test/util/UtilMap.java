package pe.test.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UtilMap {
	
	public static Map<String, Object> jsonToMap(JSONObject json) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		try {

			if (json != JSONObject.NULL) {
				retMap = toMap(json);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return retMap;
	}

	public static Map<String, Object> toMap(JSONObject object) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Iterator<String> keysItr = object.keys();
			while (keysItr.hasNext()) {
				
				String key = keysItr.next();
				Object value = object.get(key);
				
				if (!value.toString().equals("null")) {
					System.out.println("value ->"+value);
					if (value instanceof JSONArray) {
						value = toList((JSONArray) value);
					}

					else if (value instanceof JSONObject) {
						value = toMap((JSONObject) value);
					}
					map.put(key, value);
				}
				else {
					value=null;
				}
				
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public static List<Object> toList(JSONArray array) {
		List<Object> list = new ArrayList<Object>();
		try {
			for (int i = 0; i < array.length(); i++) {
				Object value = array.get(i);
				if (value instanceof JSONArray) {
					value = toList((JSONArray) value);
				}

				else if (value instanceof JSONObject) {
					value = toMap((JSONObject) value);
				}
				list.add(value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
